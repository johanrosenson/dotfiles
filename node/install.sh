#!/bin/bash

echo "Installing Node 12 (via Homebrew)"

brew list node@12x &>/dev/null

if [[ $? -ne 0 ]]; then
    brew install node@12 -v
    brew link node@12 --force
else
    echo "> Node 12 already installed"
fi
