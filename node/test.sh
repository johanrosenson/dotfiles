#!/bin/bash

echo "Testing NodeJS (expecting node v12 and npm 6)"

set -x

node -v

npm -v

{ set +x; } 2>/dev/null
