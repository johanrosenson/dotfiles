#!/bin/bash

# https://getcomposer.org/doc/faqs/how-to-install-composer-programmatically.md

ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "Installing Composer"

EXPECTED_CHECKSUM="$(wget -q -O - https://composer.github.io/installer.sig)"
php -r "copy('https://getcomposer.org/installer', '$ROOT/composer-setup.php');"
ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', '$ROOT/composer-setup.php');")"

if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]
then
    >&2 echo 'ERROR: Invalid installer checksum'
    rm "$ROOT/composer-setup.php"
    exit 1
fi

php "$ROOT/composer-setup.php" --install-dir="$ROOT"
RESULT=$?
rm "$ROOT/composer-setup.php"
#exit $RESULT

if [[ $RESULT -ne 0 ]]; then
    echo "composer-setup failed, aborting composer install"
    exit $RESULT
fi

echo "> Moving composer.phar"
mv "$ROOT/composer.phar" /usr/local/bin/composer

echo "> Composer installed"
