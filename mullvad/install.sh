#!/bin/bash

echo "Installing Mullvad VPN (via Homebrew)"

if [[ ! -d "/Applications/Mullvad VPN.app" ]]; then
    brew cask install mullvadvpn -v
else
    echo "> Mullvad VPN is already installed"
fi
