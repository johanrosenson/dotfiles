#!/bin/bash

# ~/Library/Application Support/com.tinyapp.TablePlus/Data

echo "Installing TablePlus (via Homebrew)"

if [[ ! -d "/Applications/TablePlus.app" ]]; then
    brew cask install tableplus -v
else
    echo "> TablePlus is already installed"
fi

echo "> Adding TablePlus to the Dock"

if [[ -d "/Applications/Sublime Text.app" ]]; then
    AFTER="Sublime Text"
else
    AFTER="App Store"
fi

dockutil --find "TablePlus" &>/dev/null

if [[ $? -ne 0 ]]; then
    dockutil --add /Applications/TablePlus.app --after "$AFTER"
fi
