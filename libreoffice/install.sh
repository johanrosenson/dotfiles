#!/bin/bash

# https://apple.stackexchange.com/questions/338849/libreoffice-doesnt-have-an-icon
# https://bugs.documentfoundation.org/show_bug.cgi?id=50278

echo "Installing LibreOffice (via Homebrew)"

if [[ ! -d "/Applications/LibreOffice.app" ]]; then
    brew cask install libreoffice -v
else
    echo "> LibreOffice is already installed"
fi

echo "> Adding LibreOffice to the Dock"

dockutil --find "LibreOffice" &>/dev/null

if [[ $? -ne 0 ]]; then
    if [[ -d "/Applications/LibreOffice.app" ]]; then
        dockutil --add /Applications/LibreOffice.app --position "last"
    else
        echo "> Can't add LibreOffice to the Dock, can't find /Applications/LibreOffice.app"
    fi
fi
