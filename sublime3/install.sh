#!/bin/bash

ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "Installing Sublime Text 3 (via Homebrew)"

if [[ ! -d "/Applications/Sublime Text.app" ]]; then
    brew cask install sublime-text -v
else
    echo "> Sublime Text 3 is already installed"
fi

SUBLIME_SUPPORT_DIRECTORY="$HOME/Library/Application Support/Sublime Text 3"

echo "> Provisioning Sublime Text 3 (start and close)"

if [[ ! -d $SUBLIME_SUPPORT_DIRECTORY ]]; then
    open -a "/Applications/Sublime Text.app"

    sleep 0.25

    killall "Sublime Text"
else
    echo "> Already provisioned"
fi

echo "> Installing Package Controll"

PACKAGE_CONTROL_PACKAGE="Package Control.sublime-package"
PACKAGE_CONTROL_INSTALL_PATH="$SUBLIME_SUPPORT_DIRECTORY/Installed Packages/$PACKAGE_CONTROL_PACKAGE"

if [[ ! -f $PACKAGE_CONTROL_INSTALL_PATH ]]; then
    cp "$ROOT/$PACKAGE_CONTROL_PACKAGE" "$PACKAGE_CONTROL_INSTALL_PATH"
else
    echo "> Package control is already installed"
fi

echo "> Symlinking User directory"

SUBLIME_USER_DIRECTORY="$SUBLIME_SUPPORT_DIRECTORY/Packages/User"

SYMLINK_USER_DIRECTORY="$ROOT/User"

if [[ ! -L $SUBLIME_USER_DIRECTORY ]]; then
    DATE=$(date "+%Y-%m-%d-%H%M%S")

    if [[ -d $SUBLIME_USER_DIRECTORY ]]; then
        mv "$SUBLIME_USER_DIRECTORY" "$SUBLIME_USER_DIRECTORY.bak-$DATE"
    fi

    ln -s "$SYMLINK_USER_DIRECTORY" "$SUBLIME_USER_DIRECTORY"
fi

# Add to the Dock
echo "> Adding Sublime Text 3 to the Dock"
if [[ -d "/Applications/Sublime Text.app" ]]; then
    if [[ -d "/Applications/iTerm.app" ]]; then
        AFTER="iTerm"
    elif [[ -d "/Applications/Google Chrome.app" ]]; then
        AFTER="Google Chrome"
    else
        AFTER="App Store"
    fi

    dockutil --find "Sublime Text" &>/dev/null

    if [[ $? -ne 0 ]]; then
        dockutil --add "/Applications/Sublime Text.app" --after "$AFTER"
    fi
else
    echo "> Can't add Sublime Text 3 to the dock, can't find \"/Applications/Sublime Text.app\""
fi
