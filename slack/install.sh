#!/bin/bash

echo "Installing Slack (via Homebrew)"

if [[ ! -d "/Applications/Slack.app" ]]; then
    brew cask install slack -v
else
    echo "> Slack is already installed"
fi

echo "> Adding Slack to the Dock"

if [[ -d "/Applications/TablePlus.app" ]]; then
    AFTER="TablePlus"
else
    AFTER="App Store"
fi

dockutil --find "Slack" &>/dev/null

if [[ $? -ne 0 ]]; then
    if [[ -d "/Applications/Slack.app" ]]; then
        dockutil --add /Applications/Slack.app --after "$AFTER"
    else
        echo "> Can't add Slack to the Dock, can't find /Applications/Slack.app"
    fi
fi
