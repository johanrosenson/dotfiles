#!/bin/bash

echo "Installing VLC (via Homebrew)"

if [[ ! -d "/Applications/VLC.app" ]]; then
    brew cask install vlc -v
else
    echo "> VLC is already installed"
fi
