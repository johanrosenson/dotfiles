#!/bin/bash

echo "Installing Insomnia (via Homebrew)"

if [[ ! -d "/Applications/Insomnia.app" ]]; then
    brew cask install insomnia -v
else
    echo "> Insomnia is already installed"
fi

echo "> Adding Insomnia to the Dock"

if [[ -d "/Applications/Sublime Text.app" ]]; then
    AFTER="Sublime Text"
elif [[ -d "/Applications/iTerm.app" ]]; then
    AFTER="iTerm"
elif [[ -d "/Applications/Google Chrome.app" ]]; then
    AFTER="Google Chrome"
else
    AFTER="App Store"
fi

dockutil --find "Insomnia" &>/dev/null

if [[ $? -ne 0 ]]; then
    dockutil --add /Applications/Insomnia.app --after "$AFTER"
fi
