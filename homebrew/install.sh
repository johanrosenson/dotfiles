#!/bin/bash

# https://brew.sh/

echo "Installing Homebrew"

INSTALL_HOMEBREW=0

command -v brew >/dev/null 2>&1 || {
    INSTALL_HOMEBREW=1
}

if [[ INSTALL_HOMEBREW -eq 1 ]]; then
    echo "> Running install script \"https://raw.githubusercontent.com/Homebrew/install/master/install.sh\""
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
fi

echo "> Updating Homebrew"
brew update -v

echo "> Running brew cleanup"
brew cleanup -v

echo "> Running brew upgrade"
brew cleanup -v

echo "> Installing Python 3"
brew list python &>/dev/null

if [[ $? -ne 0 ]]; then
    brew install python -v
else
    echo "> Python 3 already installed"
fi

echo "> Installing PHP 7.4"
brew list php@7.4 &>/dev/null

if [[ $? -ne 0 ]]; then
    brew install php@7.4 -v
    brew link php@7.4 --force -v
    brew services start php@7.4
else
    echo "> PHP 7.4 already installed"
fi

echo "> Installing MySQL 5.7 (+ MySQL Client 8.0)"
brew list mysql@5.7 &>/dev/null

if [[ $? -ne 0 ]]; then
    brew install mysql@5.7 -v
    brew services start mysql@5.7
else
    echo "> MySQL 5.7 already installed"
fi

brew list mysql-client@8.0 &>/dev/null

if [[ $? -ne 0 ]]; then
    brew install mysql-client@8.0 -v # compatible with MySQL Server 5.7
else
    echo "> MySQL Client 8.0 already installed"
fi

echo "> Installing Redis 6.0"
brew list redis@6.0 &>/dev/null

if [[ $? -ne 0 ]]; then
    brew install redis@6.0 -v
    brew services start redis@6.0
else
    echo "> Redis 6.0 already installed"
fi

echo "> Installing cask Java 8 (required by Elasticsearch)"
brew cask list adoptopenjdk8 &>/dev/null

if [[ $? -ne 0 ]]; then
    brew cask install homebrew/cask-versions/adoptopenjdk8 -v
else
    echo "> Java 8 is already installed"
fi

echo "> Installing Elasticsearch 6"
brew list elasticsearch@6 &>/dev/null

if [[ $? -ne 0 ]]; then
    brew install elasticsearch@6 -v
    brew services start elasticsearch@6
else
    echo "> Elasticsearch 6 already installed"
fi

echo "> Installing misc. utilities"

brew list dockutil &>/dev/null || brew install dockutil -v # used by macos/dock.sh
brew list htop &>/dev/null || brew install htop -v
brew list ncdu &>/dev/null || brew install ncdu -v
brew list nmap &>/dev/null || brew install nmap -v # todo: learn how to actually use nmap
brew list tmux &>/dev/null || brew install tmux -v
brew list wget &>/dev/null || brew install wget -v # used by composer/install.sh
