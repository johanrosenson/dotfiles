#!/bin/bash

echo "Installing .ssh/config"

if [[ ! -d ~/.ssh ]]; then
    mkdir ~/.ssh
fi

chmod 700 ~/.ssh

if [[ ! -f ~/.ssh/config ]]; then
    touch ~/.ssh/config
fi

chmod 644 ~/.ssh/config
