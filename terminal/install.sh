#!/bin/bash

ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "Configuring terminal"

# Copy .zshrc to user directory
cp "$ROOT/.zshrc" ${HOME}
