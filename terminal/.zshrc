# https://scriptingosx.com/2019/07/moving-to-zsh-06-customizing-the-zsh-prompt/
# http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html#Prompt-Expansion

# user@host:directory$
PROMPT='%B%F{green}%n@%m%f%b:%B%F{blue}%~%f%b$ '

export CLICOLOR=1
export LSCOLORS=ExGxFxdxCxDxDxhbadacec

COMPOSER_PATH="$HOME/.composer/vendor/bin"
MYSQL_CLIENT_PATH="/usr/local/opt/mysql-client/bin"
HOMEBREW_SBIN="/usr/local/sbin"
# FLUTTER_PATH="$HOME/flutter/bin"

export PATH="$PATH:$COMPOSER_PATH"
export PATH="$MYSQL_CLIENT_PATH:$PATH"
export PATH="$HOMEBREW_SBIN:$PATH"
# export PATH=$FLUTTER_PATH:$PATH

alias ls='ls -a'
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../../../'
alias ....='cd ../../../../'
