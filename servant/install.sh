#!/bin/bash

ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "Installing Servant"

sh "$ROOT/servant-cli/servant" install

echo "> Stopping services"

servant stop

echo "> Starting services"

servant start

