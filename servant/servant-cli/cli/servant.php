#!/usr/bin/env php
<?php

$command = $_SERVER['argv'][1] ?? null;

/**
 * LOGIC
 */

switch ($command) {
    case 'install':
        symlinkToUsersBin();
        break;

    case 'stop':
        stopServices();
        showStatus();
        break;

    case 'start':
        startServices();
        disableAutoStart();
        showStatus();
        break;

    case 'restart':
        restartServices();
        disableAutoStart();
        showStatus();
        break;

    case 'status':
        showStatus();
        break;

    case null:
        echo "\e[31m" . 'Missing command' . "\e[0m" . PHP_EOL;
        break;

    default:
        echo "\e[31m" . 'Unknown command "' . $command . '"' . "\e[0m" . PHP_EOL;
        break;
}

/**
 * FUNCTIONS
 */

function symlinkToUsersBin() 
{
    $binPath = '/usr/local/bin/servant';

    exec('sudo -u "' . user() . '" rm ' . $binPath . ' > /dev/null 2>&1');

    exec('sudo -u "' . user() . '" ln -s ' . realpath(__DIR__ . '/../servant') . ' ' . $binPath . ' 2>&1', $output);

    echo 'symlink ' . $binPath . "\e[32m" . ' created' . "\e[0m" . PHP_EOL;
}

function isError(?array $output) : bool 
{
    return strpos($output[0] ?? '', 'Error:') === 0 ? true : false;
}

function isRoot() : bool 
{
    return posix_getuid() === 0 ? true : false;
}

function user() 
{
    if (! isset($_SERVER['SUDO_USER'])) {
        return $_SERVER['USER'];
    }

    return $_SERVER['SUDO_USER'];
}

function assertRoot() 
{
    if(! isRoot()) {
        echo "\e[31m" . 'Run as root!' . "\e[0m" . PHP_EOL;

        die();
    }
}

function stopServices() 
{
    $services = runningServices();

    if(! count($services)) {
        echo 'No running services' . PHP_EOL;
        return;
    }

    foreach($services as $service) {
        stopService($service);
    }
}

function stopService($service) 
{
    $serviceName = $service['name'];

    echo 'Stopping ' . $serviceName . '... ';

    $output = null;

    if ($service['user'] != 'root') {
        exec('sudo -u ' . $service['user'] . ' brew services stop ' . $serviceName . ' 2>&1', $output);
    } else {
        exec('brew services stop ' . $serviceName . ' 2>&1', $output);
        if (isError($output) && preg_match('/started as `(?P<user>[^`]+)`/', $output[0], $matches)) {
            $output = null;
            exec('sudo -u ' . $matches['user'] . ' brew services stop ' . $serviceName . ' 2>&1', $output);
        }
    }

    if (isError($output)) {
        echo "\e[31m" . $output[0] . "\e[0m" . PHP_EOL;
    } else {
        echo "\e[32m" . $serviceName . ' stopped' . "\e[0m" . PHP_EOL;
    }
}

function restartServices() 
{
    assertRoot();

    $services = allServices();

    foreach ($services as $service) {
        if ($service['status'] == 'started') {
            stopService($service);
        }

        startService($service);
    }
}

function showStatus() 
{
    echo 'Fetching services status...' . PHP_EOL;

    $services = allServices();

    $cols = [];

    // Räkna ut storlekar på kolumner
    foreach ($services as $service) {
        $cols['name'] = max($cols['name'] ?? 0, mb_strlen($service['name']));
        $cols['status'] = max($cols['status'] ?? 0, mb_strlen($service['status']));
        $cols['user'] = max($cols['user'] ?? 0, mb_strlen($service['user']));
        $cols['plist'] = max($cols['plist'] ?? 0, mb_strlen($service['plist']));
    }

    // Skriv header
    foreach ($cols as $col => $size) {
        echo "\e[1m" . str_pad(ucfirst($col), $size) . "\e[0m ";
    }
    echo PHP_EOL;

    // Skriv rader
    foreach ($services as $service) {
        $statusColor = "\e[0m";
        
        if ($service['status'] == 'started') {
            $statusColor = "\e[32m";
        } elseif ($service['status'] == 'error') {
            $statusColor = "\e[31m";
        }

        echo str_pad($service['name'], $cols['name']) . ' ';
        echo $statusColor . str_pad($service['status'], $cols['status']) . "\e[0m ";
        echo str_pad($service['user'], $cols['user']) . ' ';
        echo str_pad($service['plist'], $cols['plist']) . ' ';
        echo PHP_EOL;
    }
}

function startServices() 
{
    assertRoot();

    $services = allServices();

    foreach ($services as $service) {
        startService($service);
    }
}

function startService($service) 
{
    $serviceName = $service['name'];

    echo 'Starting ' . $serviceName . '... ';

    if (runAsUser($service)) {
        exec('sudo -u "' . user() . '" brew services start ' . $serviceName . ' 2>&1', $output);
    } else {
        exec('brew services start ' . $serviceName . ' 2>&1', $output);
    }

    if (isError($output)) {
        echo "\e[31m" . $output[0] . "\e[0m" . PHP_EOL;
    } else {
        echo "\e[32m" . $serviceName . ' started' . "\e[0m" . PHP_EOL;
    }

}

function runAsUser($service) : bool 
{
    $runAsUser = [
        // 'mysql@5.7',
        'elasticsearch',
        'mysql',
    ];

    if (in_array($service['name'], $runAsUser)) {
        return true;
    }

    if (preg_match('/^(?P<serviceName>[^@]+)@(?P<version>.+)$/', $service['name'], $matches)) {
        if (in_array($matches['serviceName'], $runAsUser)) {
            return true;
        }
    }

    return false;
}

function disableAutoStart() 
{
    echo 'Disabling services autostart... ';

    $plists = [];

    $services = allServices();

    foreach ($services as $service) {

        $plist = $service['plist'];

        if (is_null($plist)) {
            continue;
        }

        if (file_exists($plist)) {
            unlink($plist);
        }

        // $content = file_get_contents($plist);

        // $content = preg_replace('/(<key>(?:RunAtLoad|KeepAlive)<\/key>\s+)<true\/>/m', '\1<false/>', $content);

        // file_put_contents($plist, $content);
    }

    echo "\e[32m" . 'disabled' . "\e[0m" . PHP_EOL;
}

function allServices() : array 
{
    $ignore = [
        // 'dnsmasq',
    ];

    $services = [];

    exec('brew services list 2>&1', $output);

    if (! isError($output)) {
        array_shift($output);

        foreach ($output as $row) {
            if (preg_match('/^(?P<name>\S+)\s+(?P<status>\S+)(?:\s+(?P<user>\S+))?(?:\s+(?P<plist>\S+\.plist))?$/', $row, $matches)) {
                if (in_array($matches['name'], $ignore)) {
                    continue;
                }

                $name = $matches['name'];

                $services[$name] = [
                    'name' => $name,
                    'status' => $matches['status'],
                    'user' => $matches['user'] ?? null,
                    'plist' => $matches['plist'] ?? null,
                ];
            }
        }
    }

    if (isRoot()) {
        $output = null;
        exec('sudo -u "' . user() . '" brew services list 2>&1', $output);

        if (! isError($output)) {
            array_shift($output);

            foreach ($output as $row) {
                if (preg_match('/^(?P<name>\S+)\s+(?P<status>\S+)(?:\s+(?P<user>\S+))?(?:\s+(?P<plist>\S+\.plist))?$/', $row, $matches)) {
                    if (in_array($matches['name'], $ignore)) {
                        continue;
                    }

                    $name = $matches['name'];

                    if ($matches['status'] == 'started') {
                        $services[$name]['status'] = $matches['status'];
                    }

                    $services[$name]['user'] = $services[$name]['user'] ?? ($matches['user'] ?? null);
                    $services[$name]['plist'] = $services[$name]['plist'] ?? ($matches['plist'] ?? null);
                }
            }
        }
    }

    return $services;
}

function runningServices() : array 
{
    $services = array_filter(allServices(), function ($service) {
        return $service['status'] === 'started' || $service['plist'] !== null ? true : false;
    });

    return $services;
}
