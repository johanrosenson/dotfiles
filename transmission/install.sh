#!/bin/bash

echo "Installing Transmission (via Homebrew)"

if [[ ! -d "/Applications/Transmission.app" ]]; then
    brew cask install transmission -v
else
    echo "> Transmission is already installed"
fi
