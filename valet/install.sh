#!/bin/bash

composer global require laravel/valet

valet install

valet tld devlop

if [[ ! -d ~/valet ]]; then
    mkdir ~/valet
fi

echo "> Changing directory to ~/valet"

cd ~/valet

echo "> Running \"valet park\" in ~/valet"

valet park
