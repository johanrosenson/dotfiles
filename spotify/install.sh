#!/bin/bash

echo "Installing Spotify (via Homebrew)"

if [[ ! -d "/Applications/Spotify.app" ]]; then
    brew cask install spotify -v
else
    echo "> Spotify is already installed"
fi

echo "> Adding Spotify to the Dock"

AFTER="Notes"

dockutil --find "Spotify" &>/dev/null

if [[ $? -ne 0 ]]; then
    if [[ -d "/Applications/Spotify.app" ]]; then
        dockutil --add /Applications/Spotify.app --after "$AFTER"
    else
        echo "> Can't add Spotify to the Dock, can't find /Applications/Spotify.app"
    fi
fi
