#!/bin/bash

# https://github.com/diimdeep/dotfiles/blob/master/osx/configure/hotkeys.sh

echo "Configuring macOS (Keyboard)"

# Disable smart quotes as they’re annoying when typing code
defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false

# Change the keyboard shortcut for cycling through windows (CMD+§)
defaults write com.apple.symbolichotkeys.plist AppleSymbolicHotKeys -dict-add 27 "
  <dict>
    <key>enabled</key><true/>
    <key>value</key><dict>
      <key>type</key><string>standard</string>
      <key>parameters</key>
      <array>
        <integer>167</integer>
        <integer>10</integer>
        <integer>1048576</integer>
      </array>
    </dict>
  </dict>
"