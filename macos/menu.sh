#!/bin/bash

echo "Configuring macOS (UI)"

# Show battery percentage in menu
defaults write com.apple.menuextra.battery ShowPercent -string "YES"

# Show the volume in menu bar
defaults write com.apple.systemuiserver "NSStatusItem Visible com.apple.menuextra.volume" -int 1
defaults write com.apple.systemuiserver menuExtras -array-add "/System/Library/CoreServices/Menu Extras/Volume.menu"

# Remove "Fast user switching menu"
defaults write com.apple.systemuiserver "NSStatusItem Visible com.apple.menuextra.appleuser" -int 0

menuExtras=`defaults read com.apple.systemuiserver menuExtras`

# remove leading opening paranthesis
menuExtras="${menuExtras#*\(}"

# remove trailing opening paranthesis
menuExtras="${menuExtras%*\)}"

userPattern="User\.menu\"$"

defaults delete com.apple.systemuiserver menuExtras

while IFS=, read value; do
    if [[ $value == "" ]]; then
        continue
    fi

    if [[ $value =~ $userPattern ]]; then
        continue
    fi

    # echo $value;
    defaults write com.apple.systemuiserver menuExtras -array-add "$value"
done <<< "$menuExtras"

# Manually restore the menuExtras using:
# defaults write com.apple.systemuiserver menuExtras -array "/System/Library/CoreServices/Menu Extras/Clock.menu" "/System/Library/CoreServices/Menu Extras/Battery.menu" "/System/Library/CoreServices/Menu Extras/AirPort.menu" "/System/Library/CoreServices/Menu Extras/Displays.menu" "/System/Library/CoreServices/Menu Extras/User.menu"

# Restart the top menu
killall SystemUIServer &>/dev/null
