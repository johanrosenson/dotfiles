#!/bin/bash

echo "Configuring macOS (Trackpad)"

# Enable Tap to click
echo "> Enabling \"Tap to click\""

defaults write com.apple.AppleMultitouchTrackpad Clicking -bool true
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults -currentHost write 'Apple Global Domain' com.apple.mouse.tapBehavior 1

# Change Secondary click to bottom right corner of trackpad (does not appear to be working)
# https://github.com/mathiasbynens/dotfiles/blob/master/.macos#L129
# https://apple.stackexchange.com/a/48200

echo "> \"Rightclick in bottom right corner\" must be manually enabled in Trackpad preferences"

# defaults write com.apple.AppleMultiTouchTrackpad TrackpadRightClick -bool true
# defaults write com.apple.AppleMultiTouchTrackpad TrackpadCornerSecondaryClick -int 2

# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadRightClick -bool true
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadCornerSecondaryClick -int 2

# defaults -currentHost write 'Apple Global Domain' com.apple.trackpad.enableSecondaryClick -bool true
# defaults -currentHost write 'Apple Global Domain' com.apple.trackpad.trackpadCornerClickBehavior -int 1

# defaults -currentHost write NSGlobalDomain com.apple.trackpad.trackpadCornerClickBehavior -int 1
# defaults -currentHost write NSGlobalDomain com.apple.trackpad.enableSecondaryClick -bool true

# Disable 3 finger "Look up & data detectors" (does not appear to be working)
echo "> \"Rightclick in bottom right corner\" must be manually disabled in Trackpad preferences"

# defaults write com.apple.AppleMultitouchTrackpad TrackpadThreeFingerTapGesture -int 0
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadThreeFingerTapGesture -int 0

# Restart the Trackpad
echo "> Login and logout for the Trackpad changes to take effect"
# Todo: figure out how to restart from the Terminal
