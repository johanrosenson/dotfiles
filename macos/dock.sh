#!/bin/bash

#ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "Configuring macOS (Dock)"

# Don’t animate opening applications from the dock
defaults write com.apple.dock launchanim -bool false

# Changing pages in Launchpad (remove animation)
defaults write com.apple.dock springboard-page-duration -float 0

# Showing and hiding Launchpad (remove animation)
defaults write com.apple.dock springboard-show-duration -float 0
defaults write com.apple.dock springboard-hide-duration -float 0

# Remove the auto-hiding dock delay
defaults write com.apple.dock autohide-delay -float 0

# Automatically hide and show the Dock
defaults write com.apple.dock autohide -bool true

# Don’t show recent applications in Dock
defaults write com.apple.dock show-recents -bool false

# Minimize windows into their application’s icon
defaults write com.apple.dock minimize-to-application -bool true

# Showing and hiding Mission Control, command+numbers
defaults write com.apple.dock expose-animation-duration -float 0

# Minimize/expand windows using scale effect
defaults write com.apple.dock mineffect scale

# Remove the animation when hiding/showing the dock
defaults write com.apple.dock autohide-time-modifier -float 0

# Remove trash items from the dock
# Export the current dock using:
# defaults read com.apple.dock persistent-apps > dock-apps.txt
# defaults read com.apple.dock persistent-others > dock-others.txt
dockutil --remove 'com.apple.AddressBook' &>/dev/null
dockutil --remove 'com.apple.FaceTime' &>/dev/null
dockutil --remove 'com.apple.iCal' &>/dev/null
dockutil --remove 'com.apple.iChat' &>/dev/null
dockutil --remove 'com.apple.Maps' &>/dev/null
dockutil --remove 'com.apple.Music' &>/dev/null
dockutil --remove 'com.apple.Photos' &>/dev/null
dockutil --remove 'com.apple.podcasts' &>/dev/null
dockutil --remove 'com.apple.reminders' &>/dev/null
dockutil --remove 'com.apple.Safari' &>/dev/null
dockutil --remove 'com.apple.siri.launcher' &>/dev/null
dockutil --remove 'com.apple.systempreferences' &>/dev/null
dockutil --remove 'com.apple.TV' &>/dev/null

dockutil --remove 'Downloads' &>/dev/null

dockutil --remove 'Numbers' &>/dev/null
dockutil --remove 'Keynote' &>/dev/null
dockutil --remove 'Pages' &>/dev/null

dockutil --move 'App Store' --after "Launchpad" &>/dev/null

# Add common Applications to the Dock
if [[ -d "/Applications/Google Chrome.app" ]]; then
    dockutil --find "Google Chrome" &>/dev/null

    if [[ $? -ne 0 ]]; then
        dockutil --add "/Applications/Google Chrome.app" --after "App Store"
    fi
fi

if [[ -d "/Applications/GitKraken.app" ]]; then
    if [[ -d "/Applications/Google Chrome.app" ]]; then
        AFTER="Google Chrome"
    else
        AFTER="App Store"
    fi

    dockutil --find "GitKraken" &>/dev/null

    if [[ $? -ne 0 ]]; then
        dockutil --add "/Applications/GitKraken.app" --after "$AFTER"
    fi
fi

if [[ -d "/System/Applications/Calculator.app" ]]; then
    dockutil --find "Calculator" &>/dev/null

    if [[ $? -ne 0 ]]; then
        dockutil --add "/System/Applications/Calculator.app" --after "Mail"
    fi
fi

# Restart Dock
killall Dock &>/dev/null
