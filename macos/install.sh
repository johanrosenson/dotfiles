#!/bin/bash

# https://ss64.com/osx/defaults.html
# https://brandonb.ca/os-x-for-hackers-coders
# https://pawelgrzybek.com/change-macos-user-preferences-via-command-line/

ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "Configuring macOS"

sh "$ROOT/computer-name.sh"
sh "$ROOT/dock.sh"
sh "$ROOT/finder.sh"
sh "$ROOT/general.sh"
sh "$ROOT/hot-corners.sh"
sh "$ROOT/keyboard.sh"
sh "$ROOT/menu.sh"
sh "$ROOT/security.sh"
sh "$ROOT/spotlight.sh"
sh "$ROOT/trackpad.sh"
