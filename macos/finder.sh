#!/bin/bash

echo "Configuring macOS (Finder)"

# Expand the save panel by default
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint -bool true
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint2 -bool true

# Show all files in Finder
defaults write com.apple.finder AppleShowAllFiles -bool true

# Set Home as the default location since that is the only reasonable option
defaults write com.apple.finder NewWindowTarget -string "PfHm"
# For other paths, use NewWindowTarget `PfLo` and newWindowTargetPath with `file:///full/path/here/`
# defaults write com.apple.finder NewWindowTargetPath -string "file://${HOME}/Desktop/"

# Display full POSIX path as Finder window title
defaults write com.apple.finder _FXShowPosixPathInTitle -bool true

# Show status bar in Finder
defaults write com.apple.finder ShowStatusBar -bool true

# Show path bar in Finder (does not appear to be working? change to false to see proof)
defaults write com.apple.finder ShowPathbar -bool true

# Keep folders on top in Finder when sorting by name
defaults write com.apple.finder _FXSortFoldersFirst -bool true

# Disable the warning when changing a file extension
defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

# Open folders in new windows instead of tabs
defaults write com.apple.finder FinderSpawnTab -bool false

# Enable spring loading for directories
defaults write NSGlobalDomain com.apple.springing.enabled -bool true

# Remove the spring loading delay for directories
defaults write NSGlobalDomain com.apple.springing.delay -float 0.0

# Show the ~/Library folder
chflags nohidden ~/Library && xattr -d com.apple.FinderInfo ~/Library &>/dev/null

# Show the /Volumes folder
# https://unix.stackexchange.com/a/438359
VOLUMES_FLAGS=$(stat -f "%Xf" /Volumes)

if [[ $(($VOLUMES_FLAGS & 0x40)) != 0 ]]; then
    sudo chflags nohidden /Volumes
fi

# Avoid creating .DS_Store files on network or USB volumes
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true

# Restart Finder
killall Finder
