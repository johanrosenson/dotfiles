#!/bin/bash

echo "Configuring macOS (Security)"

# Require password immediately after sleep or screen saver begins
# Broken since 10.13, must be done using a profile now
# defaults write com.johan.config.screensaver askForPassword -bool true
# defaults write com.johan.config.screensaver askForPasswordDelay -int 0
echo "> Remember to manually change password delay to immediately in Security & Privacy"

# Privacy: don’t send search queries to Apple
defaults write com.apple.Safari UniversalSearchEnabled -bool false
defaults write com.apple.Safari SuppressSearchSuggestions -bool true
