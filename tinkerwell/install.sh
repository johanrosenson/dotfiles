#!/bin/bash

echo "Installing Tinkerwell (via Homebrew)"

if [[ ! -d "/Applications/Tinkerwell.app" ]]; then
    brew cask install tinkerwell -v
else
    echo "> Tinkerwell is already installed"
fi

echo "> Adding Tinkerwell to the Dock"

if [[ -d "/Applications/Insomnia.app" ]]; then
    AFTER="Insomnia"
else
    AFTER="App Store"
fi

dockutil --find "Tinkerwell" &>/dev/null

if [[ $? -ne 0 ]]; then
    if [[ -d "/Applications/Tinkerwell.app" ]]; then
        dockutil --add /Applications/Tinkerwell.app --after "$AFTER"
    else
        echo "> Can't add Tinkerwell to the Dock, can't find /Applications/Tinkerwell.app"
    fi
fi
