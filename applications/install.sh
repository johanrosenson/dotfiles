#!/bin/bash

ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "Installing Applications"

sh "$ROOT/rectangle.sh"
sh "$ROOT/microsoft-azure-storage-explorer.sh"
sh "$ROOT/the-unarchiver.sh"
