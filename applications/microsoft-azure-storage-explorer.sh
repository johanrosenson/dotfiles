#!/bin/bash

echo "> Microsoft Azure Storage Explorer (homebrew cask)"

if [[ ! -d "/Applications/Microsoft Azure Storage Explorer.app" ]]; then
    brew cask install microsoft-azure-storage-explorer -v
else
    echo "> Microsoft Azure Storage Explorer is already installed"
fi
