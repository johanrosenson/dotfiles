#!/bin/bash

echo "> Installing Rectangle (homebrew cask)"

if [[ ! -d "/Applications/Rectangle.app" ]]; then
    brew cask install rectangle -v
else
    echo "> Rectangle is already installed"
fi

open -a "/Applications/Rectangle.app"

# Todo: configure
# Clear all shortcuts except:
# Left half: Ctrl + Shift + Left
# Right half: Ctrl + Shift + Right
# Maximize: Ctrl + Shift + Up
# Almost Maximize: Ctrl + Shift + Down
