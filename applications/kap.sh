#!/bin/bash

echo "> Kap (homebrew cask)"

if [[ ! -d "/Applications/Kap.app" ]]; then
    brew cask install kap -v
else
    echo "> Kap is already installed"
fi
