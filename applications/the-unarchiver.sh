#!/bin/bash

echo "> Installing The Unarchiver (homebrew cask)"

if [[ ! -d "/Applications/The Unarchiver.app" ]]; then
    brew cask install the-unarchiver -v
else
    echo "> The Unarchiver is already installed"
fi
