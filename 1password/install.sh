#!/bin/bash

echo "Installing 1Password (via Homebrew)"

if [[ ! -d "/Applications/1Password 7.app" ]]; then
    brew cask install 1password -v
else
    echo "> 1Password is already installed"
fi

echo "> Adding 1Password to the Dock"

dockutil --find "1Password 7" &>/dev/null

if [[ $? -ne 0 ]]; then
    if [[ -d "/Applications/1Password 7.app" ]]; then
        dockutil --add "/Applications/1Password 7.app" --after "Insomnia"
    else
        echo "> Can't add 1Password to the Dock, can't find /Applications/1Password 7.app"
    fi
fi
