#!/bin/bash

echo "Installing Charles (via Homebrew)"

if [[ ! -d "/Applications/Charles.app" ]]; then
    brew cask install charles -v
else
    echo "> Charles is already installed"
fi
