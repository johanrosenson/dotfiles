#!/bin/bash

# https://github.com/mas-cli/mas
# Use "mas list" to list currently installed applications (sometimes have to run twice to get newly installed applications)

# Todo: only open -a if application is not already running

echo "Installing from Mac App Store"

# First install the mas-cli
brew list mas &>/dev/null || brew install mas -v

DATO=1470584107
FILEZILLA=1458095236
# HOUR=569089415
MICRO_SNITCH=972028355

mas install "$FILEZILLA"
dockutil --find "FileZilla" &>/dev/null

echo "> Adding FileZille to the Dock"

if [[ $? -ne 0 ]]; then
    if [[ -d "/Applications/FileZilla.app" ]]; then
        dockutil --add /Applications/FileZilla.app --before "LibreOffice"
    else
        echo "> Can't add FileZilla to the Dock, can't find /Applications/FileZilla.app"
    fi
fi

# mas install "$HOUR" # Todo: Configure automatically
# open -a "/Applications/Hour.app"

mas install "$DATO"
open -a "/Applications/Dato.app"

mas install "$MICRO_SNITCH" # Todo: Configure automatically
open -a "/Applications/Micro Snitch.app"
