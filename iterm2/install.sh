#!/bin/bash

ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "Configuring iTerm2"

# PREFERENCES="$ROOT/com.googlecode.iterm2.plist"

# https://github.com/gnachman/iTerm2/blob/8cae791fc73e1f116e57b4b4f7cfb1f39b83a880/sources/iTermPreferences.m#L42

defaults write com.googlecode.iterm2 LoadPrefsFromCustomFolder -int 1
defaults write com.googlecode.iterm2 PrefsCustomFolder -string "$ROOT"
defaults write com.googlecode.iterm2 NoSyncNeverRemindPrefsChangesLostForFile_selection -int 0
defaults write com.googlecode.iterm2 NoSyncNeverRemindPrefsChangesLostForFile -bool true

# Add to the Dock
echo "> Adding iTerm2 to the Dock"
if [[ -d /Applications/iTerm.app ]]; then
    if [[ -d "/Applications/Google Chrome.app" ]]; then
        AFTER="Google Chrome"
    elif [[ -d "/Applications/GitKraken.app" ]]; then
        AFTER="GitKraken"
    else
        AFTER="App Store"
    fi

    dockutil --find "iTerm" &>/dev/null

    if [[ $? -ne 0 ]]; then
    	dockutil --add /Applications/iTerm.app --after "$AFTER"
    fi
else
    echo "> Can't add iTerm2 to the dock, can't find /Applications/iTerm.app"
fi
