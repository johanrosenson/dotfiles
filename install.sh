#!/bin/bash

sh ./ssh/install.sh

xcode-select -p &>/dev/null

if [[ "$?" -eq 2 ]]; then
    sh ./xcode/install.sh
    echo "re-run the install script after the xcode install is finished!"
    exit 0
fi

sh ./macos/install.sh
sh ./terminal/install.sh
sh ./iterm2/install.sh
sh ./sublime3/install.sh

sh ./homebrew/install.sh
sh ./composer/install.sh
sh ./valet/install.sh
sh ./servant/install.sh
sh ./node/install.sh

sh ./applications/install.sh

sh ./mullvad/install.sh
sh ./insomnia/install.sh
sh ./tinkerwell/install.sh
sh ./tableplus/install.sh
sh ./slack/install.sh
sh ./discord/install.sh
sh ./spotify/install.sh
sh ./1password/install.sh
sh ./libreoffice/install.sh
sh ./app-store/install.sh
sh ./charles/install.sh
sh ./hex-fiend/install.sh
sh ./transmission/install.sh
sh ./vlc/install.sh
