#!/bin/bash

echo "Installing Hex Fiend (via Homebrew)"

if [[ ! -d "/Applications/Hex Fiend.app" ]]; then
    brew cask install hex-fiend -v
else
    echo "> Hex Fiend is already installed"
fi
