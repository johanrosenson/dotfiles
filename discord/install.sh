#!/bin/bash

echo "Installing Discord (via Homebrew)"

if [[ ! -d "/Applications/Discord.app" ]]; then
    brew cask install discord -v
else
    echo "> Discord is already installed"
fi

echo "> Adding Discord to the Dock"

if [[ -d "/Applications/Slack.app" ]]; then
    AFTER="Slack"
else
    AFTER="App Store"
fi

dockutil --find "Discord" &>/dev/null

if [[ $? -ne 0 ]]; then
    if [[ -d "/Applications/Discord.app" ]]; then
        dockutil --add /Applications/Discord.app --after "$AFTER"
    else
        echo "> Can't add Discord to the Dock, can't find /Applications/Discord.app"
    fi
fi
